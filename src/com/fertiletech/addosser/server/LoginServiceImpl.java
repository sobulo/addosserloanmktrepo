package com.fertiletech.addosser.server;

import com.fertiletech.addosser.client.LoginService;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.shared.LoginInfo;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class LoginServiceImpl extends RemoteServiceServlet implements
		LoginService {

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.GreetingService#login(java.lang.String)
	 */
	@Override
	public LoginInfo login(String requestUrl) {
		LoginInfo info = LoginHelper.brokeredLogin(requestUrl);
		return info;
	}
}
