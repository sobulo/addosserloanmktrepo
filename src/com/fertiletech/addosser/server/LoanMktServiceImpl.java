/**
 * 
 */
package com.fertiletech.addosser.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.client.gui.util.table.TableMessage;
import com.fertiletech.addosser.client.gui.util.table.TableMessageHeader;
import com.fertiletech.addosser.client.gui.util.table.TableMessageHeader.TableMessageContent;
import com.fertiletech.addosser.server.loanmktdata.ApplicationParameters;
import com.fertiletech.addosser.server.loanmktdata.ConsumerLoanSalesLead;
import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
@SuppressWarnings("serial")
public class LoanMktServiceImpl extends RemoteServiceServlet implements LoanMktService
{
	
	private static final Logger log =
        Logger.getLogger(LoanMktServiceImpl.class.getName());

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#createSaleLead(java.lang.String, java.lang.String, java.lang.String, java.lang.Double, java.lang.String, java.lang.Double)
	 */
	@Override
	public String createSaleLead(String phoneNumber, String companyName,
			String fullName, Double monthlySalary, String email,
			Double requestedAmount) 
	{	
		ConsumerLoanSalesLead salesLead = LoanMktDAO.createConsumerLoanSalesLead(phoneNumber, companyName, fullName, monthlySalary, email, requestedAmount);
		String msg = "<center style='color:green'>Thank you " + fullName + ", your pre-loan application has been submitted. " +
				"A representative from the consumer loans unit will contact you</center>";
		ServiceImplUtilities.logSystemUpdate(log, salesLead.getKey(), "sales lead triggered by: " + fullName + "/" + email);
		return msg;
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#updateParameters(java.util.HashMap)
	 */
	@Override
	public void updateParameters(HashMap<String, String> parameters) throws MissingEntitiesException {
		Key<ApplicationParameters> paramKey = ServiceImplUtilities.getLoanAppConfigurationKey();
		LoanMktDAO.updateApplicationParameters(paramKey, parameters);
		ServiceImplUtilities.logUserUpdate(log, paramKey);
		
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getParameters()
	 */
	@Override
	public HashMap<String, String> getParameters() {
		return LoanMktDAO.getLoanMktParams();
	}

	/* (non-Javadoc)
	 * @see com.fertiletech.addosser.client.LoanMktService#getSaleLeads(java.util.Date, java.util.Date)
	 */
	@Override
	public List<TableMessage> getSaleLeads(Date startDate, Date endDate) {
		ArrayList<TableMessage> result = new ArrayList<TableMessage>();
		//build header
		TableMessageHeader header = new TableMessageHeader(7);
		header.setText(0, "Name", TableMessageContent.TEXT);
		header.setText(1, "Email", TableMessageContent.TEXT);
		header.setText(2, "Phone", TableMessageContent.TEXT);
		header.setText(3, "Company", TableMessageContent.TEXT);
		header.setText(4, "Salary", TableMessageContent.NUMBER);
		header.setText(5, "Principal", TableMessageContent.NUMBER);
		header.setText(6, "Date", TableMessageContent.DATE);
		result.add(header);
		
		//add rows
		QueryResultIterable<ConsumerLoanSalesLead> saleLeads = LoanMktDAO.getSalesLeadsByDate(startDate, endDate);
		for(ConsumerLoanSalesLead sl : saleLeads)
		{
			TableMessage row = new TableMessage(4, 2, 1);
			row.setText(0, sl.getFullName());
			row.setText(1, sl.getEmail());
			row.setText(2, sl.getPhoneNumber());
			row.setText(3, sl.getCompanyName());
			row.setNumber(0, sl.getMonthlySalary());
			row.setNumber(1, sl.getRequestedAmount());
			row.setDate(0, sl.getDateCreated());
			result.add(row);
		}
		return result;
	}
	

}
