/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.Date;

import javax.persistence.Id;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Cached;
import com.googlecode.objectify.annotation.Indexed;
import com.googlecode.objectify.annotation.Unindexed;

/**
 * @author Segun Razaq Sobulo
 *
 */
@Unindexed
@Cached
public class ConsumerLoanSalesLead {
	
	@Id
	Long key;
	
	//customer fields
	String phoneNumber;
	String companyName;
	String fullName;
	Double monthlySalary;
	@Indexed String email;
	Double requestedAmount;
	
	//staff-ops fields
	boolean saleClosed;
	boolean activeStatus;
	String comments;
	
	//auto-populated fields
	Date dateCreated;
	@Indexed Date dateUpdated;
	String updateUser;	
	
	ConsumerLoanSalesLead(){} //objectify requires empty constructor
	
	/**
	 * @param phoneNumber
	 * @param companyName
	 * @param fullName
	 * @param monthlySalary
	 * @param email
	 * @param requestedAmount
	 */
	ConsumerLoanSalesLead(String phoneNumber, String companyName,
			String fullName, Double monthlySalary, String email,
			Double requestedAmount) 
	{
		this.phoneNumber = phoneNumber;
		this.companyName = companyName;
		this.fullName = fullName;
		this.monthlySalary = monthlySalary;
		this.email = email;
		this.requestedAmount = requestedAmount;
		this.dateCreated = this.dateUpdated = new Date(); //set to current date
		this.activeStatus = true;
		this.saleClosed = false;
		this.updateUser = null;
		this.comments = null;
		System.out.println("dateUpdated: " + dateUpdated);
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Double getMonthlySalary() {
		return monthlySalary;
	}
	public void setMonthlySalary(Double monthlySalary) {
		this.monthlySalary = monthlySalary;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getRequestedAmount() {
		return requestedAmount;
	}
	public void setRequestedAmount(Double requestedAmount) {
		this.requestedAmount = requestedAmount;
	}
	public boolean isSaleClosed() {
		return saleClosed;
	}
	public void setSaleClosed(boolean saleClosed) {
		this.saleClosed = saleClosed;
	}
	public boolean isActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	
	public Key<ConsumerLoanSalesLead> getKey()
	{
		return new Key<ConsumerLoanSalesLead>(ConsumerLoanSalesLead.class, key);
	}
}
