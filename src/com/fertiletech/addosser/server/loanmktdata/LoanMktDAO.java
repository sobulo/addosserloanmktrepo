/**
 * 
 */
package com.fertiletech.addosser.server.loanmktdata;

import java.util.Date;
import java.util.HashMap;
import java.util.logging.Logger;

import com.fertiletech.addosser.server.ServiceImplUtilities;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.google.appengine.api.datastore.QueryResultIterable;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoanMktDAO {
	private static final Logger log =
        Logger.getLogger(LoanMktDAO.class.getName());
	
	private static boolean isOfyRegistered = false; 
	
	static
	{
		registerClassesWithObjectify();
	}
	
	//objectify requires we register all classes whose object instances will/can be 
	//persisted in appengine's native datastore
	
	public static void registerClassesWithObjectify()
	{
	    if(isOfyRegistered)
	        return;
	    
	    log.warning("Registering services with Objectify");
	    ObjectifyService.register(ApplicationParameters.class);
	    ObjectifyService.register(ConsumerLoanSalesLead.class);
	    isOfyRegistered = true;
	}
	
	public static HashMap<String, String> getLoanMktParams()
	{
		Objectify ofy = ObjectifyService.begin();
		ApplicationParameters paramObject = ofy.get(ServiceImplUtilities.getLoanAppConfigurationKey());
		return paramObject.getParams();		
	}
	
	public static ApplicationParameters createApplicationParameters(String id, HashMap<String, String> parameters) throws DuplicateEntitiesException
	{
		ApplicationParameters paramsObject= new ApplicationParameters(id, parameters);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			Key<ApplicationParameters> existingKey = paramsObject.getKey();
			ApplicationParameters temp = ofy.find(existingKey);
			if(temp != null)
				ServiceImplUtilities.logAndThrowDuplicateException(log, "Application parameter already exists", existingKey);
			else
			{
				ofy.put(paramsObject);
				ofy.getTxn().commit();
			}
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return paramsObject;
	}
	
	public static void updateApplicationParameters(Key<ApplicationParameters> key, HashMap<String, String> parameters) throws MissingEntitiesException
	{
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			ApplicationParameters paramsObject = ofy.find(key);
			if(paramsObject == null)
				ServiceImplUtilities.logAndThrowMissingEntityException(log, 
						"Application parameter already exists", key);
			else
			{
				paramsObject.setParams(parameters);
				ofy.put(paramsObject);
				ofy.getTxn().commit();
			}
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
	}	
	
	public static ConsumerLoanSalesLead createConsumerLoanSalesLead(String phoneNumber, String companyName,
			String fullName, Double monthlySalary, String email, Double requestedAmount)
	{
		ConsumerLoanSalesLead salesLead = new ConsumerLoanSalesLead(phoneNumber, companyName, fullName, monthlySalary, email, requestedAmount);
		Objectify ofy = ObjectifyService.beginTransaction();
		try
		{
			ofy.put(salesLead);
			ofy.getTxn().commit();
		}
		finally
		{
			if(ofy.getTxn().isActive())
				ofy.getTxn().rollback();
		}
		return salesLead;
	}
		
	public static QueryResultIterable<ConsumerLoanSalesLead> getSalesLeadsByDate(Date startDate, Date endDate)
	{
		Objectify ofy = ObjectifyService.begin();
		return ofy.query(ConsumerLoanSalesLead.class).filter("dateUpdated >=", startDate).
		filter("dateUpdated <=", endDate).fetch();
	}
}
