/**
 * 
 */
package com.fertiletech.addosser.server;

import java.util.logging.Logger;

import com.fertiletech.addosser.server.loanmktdata.ApplicationParameters;
import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.googlecode.objectify.Key;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class ServiceImplUtilities {
	public static void logAndThrowDuplicateException(Logger log, String msgPrefix, Key<?> objectifyKey) throws DuplicateEntitiesException
	{
		String msg = msgPrefix + ". Attempt to create a duplicate object with Key: ";
		log.warning(msg + objectifyKey);
		throw new DuplicateEntitiesException(msg + objectifyKey.getName());
	}

	public static void logAndThrowMissingEntityException(Logger log, String msgPrefix, Key<?> objectifyKey) throws MissingEntitiesException
	{
		String msg = msgPrefix + ".  object not found - Key is: ";
		log.warning(msg + objectifyKey);
		throw new MissingEntitiesException(msg + objectifyKey.getName());
	}	
	
	public static void logUserUpdate(Logger log, Key<?> objectifyKey)
	{
		log.warning(objectifyKey + " updated by: " + LoginHelper.getLoggedInUser());
	}
	
	public static void logSystemUpdate(Logger log, Key<?> objectifyKey, String msg)
	{
		log.warning(objectifyKey + " updated by system: " + msg);
	}
	
	public static Key<ApplicationParameters> getLoanAppConfigurationKey()
	{
		return new Key<ApplicationParameters>(ApplicationParameters.class, EntityConstants.MARKETING_PARAM_ID);
	}	

}
