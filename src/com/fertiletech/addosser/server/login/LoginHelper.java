/**
 * 
 */
package com.fertiletech.addosser.server.login;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import com.fertiletech.addosser.shared.LoginInfo;
import com.fertiletech.addosser.shared.LoginRoles;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.google.apphosting.api.ApiProxy;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoginHelper {
	private static final Logger log = Logger.getLogger(LoginHelper.class.getName());
	private static final LoginProvider PROVIDER = new GoogleLoginProvider();
	
    public static LoginInfo brokeredLogin(String requestUri)
    {
    	LoginInfo loginInfo = new LoginInfo();
        if (PROVIDER.obtainedToken()) //logged into google?
        {
        	//ok so passed the google check, 
        	//now let's populate with additional information
        	PROVIDER.populateLoginInformation(loginInfo);
        	loginInfo.setLoggedIn(true); //gwt client should check loginInfo.role as well
            loginInfo.setLogoutUrl(PROVIDER.getCheckoutUrl(requestUri));
            loginInfo.setMessage("Welcome " + loginInfo.getName() + ", you can logout using link below once" +
            		" you're done. <ul><li><a href='" + loginInfo.getLogoutUrl() + "'>Log Out</a></li></ul>");
            
            log.warning("User: " + loginInfo.getLoginID() + " logged in as " + loginInfo.getRole());
        } 
        else 
        {
            loginInfo.setLoggedIn(false);;
            
            //to support multiple login types (google, facebook etc), this would need to set multiple urls
            loginInfo.setLoginUrl(PROVIDER.getCheckInUrl(requestUri));
            loginInfo.setMessage("Login using link below. <ul><li><a href='" + loginInfo.getLoginUrl() + "'>Log In</a></li></ul>");
        }
        return loginInfo;
    }
        
    private static class GoogleLoginProvider implements LoginProvider, Serializable 
    {
    	private final static String PROVIDER_NAME = "Google Login";
        @Override
        public String getCheckoutUrl(String url) {
            return UserServiceFactory.getUserService().createLogoutURL(url);
        }

        @Override
        public String getCheckInUrl(String url) {
            return UserServiceFactory.getUserService().createLoginURL(url);
        }

        @Override
        public String getProviderName() {
            return PROVIDER_NAME;
        }

        @Override
        public boolean obtainedToken() {
            return UserServiceFactory.getUserService().isUserLoggedIn();
        }

        @Override
        public void populateLoginInformation(LoginInfo loginInfo)
        {
            User user = UserServiceFactory.getUserService().getCurrentUser();
            String email = user.getEmail().toLowerCase();
            loginInfo.setName(user.getNickname());
            loginInfo.setLoginID(email);
            loginInfo.setRole(getRole(email));
        }
        
        public String getUserId()
        {
        	 return UserServiceFactory.getUserService().getCurrentUser().getEmail().toLowerCase();
        }
    }
    
    private static LoginRoles getRole(String email)
    {
        if(SystemProperty.environment.value() == SystemProperty.Environment.Value.Production)
        {
        	//recommended way by Google for checking user's organization
	    	String domain = (String) ApiProxy.getCurrentEnvironment().getAttributes().get("com.google.appengine.api.users.UserService.user_organization");
	    	if(domain.equalsIgnoreCase(LoginConstants.COMPANY_DOMAIN))
	    		return LoginRoles.ROLE_ADDOSSER_STAFF;
	    	else if(domain.equals(LoginConstants.FTBS_DOMAIN))
	    		return LoginRoles.ROLE_SUPER_ADMIN;
	    	else
	    		return LoginRoles.ROLE_PUBLIC;
        }
        else
        {
        	//organization info not available in dev mode, so we use logic below
        	//note that this logic works even in prod but to be on the safe side we use
        	//recommended approach for prod above
            if (email.endsWith(LoginConstants.FTBS_DOMAIN))
            	return LoginRoles.ROLE_SUPER_ADMIN;
            else if(email.endsWith(LoginConstants.COMPANY_DOMAIN))
            	return LoginRoles.ROLE_ADDOSSER_STAFF;
            else
            	return	LoginRoles.ROLE_PUBLIC;        	
        }
    }
    
    public static String getLoggedInUser()
    {
    	//this breaks if code change is made to support multiple providers
    	return ((GoogleLoginProvider) PROVIDER).getUserId();
    }
    
    //interface provided in case there is a need to support other forms of login besides
    //google in the future (e.g. open id), for now it's just an overhead since
    //we only support google login
    static interface LoginProvider 
    {
        String getCheckoutUrl(String url);
        String getCheckInUrl(String url);
        String getProviderName();
        boolean obtainedToken();
        void populateLoginInformation(LoginInfo login);
    }
}
