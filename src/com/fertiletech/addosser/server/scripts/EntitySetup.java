/**
 * 
 */
package com.fertiletech.addosser.server.scripts;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fertiletech.addosser.server.loanmktdata.ApplicationParameters;
import com.fertiletech.addosser.server.loanmktdata.EntityConstants;
import com.fertiletech.addosser.server.loanmktdata.LoanMktDAO;
import com.fertiletech.addosser.server.login.LoginHelper;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.exceptions.DuplicateEntitiesException;



/**
 * @author Segun Razaq Sobulo
 *
 */
public class EntitySetup extends HttpServlet{
	private static final Logger log = Logger.getLogger(EntitySetup.class.getName());
	
	/**
	 * web.xml contains entries to ensure only registered developers for the app can execute
	 * this script. Does not go through login logic 
	 */
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		ServletOutputStream out = res.getOutputStream();
		res.setContentType("text/html");
		try
		{
			String type = req.getParameter("type");
			if(type == null)
			{
				out.println("<b><font color='red'>Please specify a type</font></b>");
				return;
			}
			out.println("<b>setup starting</b><br/>");
			if(type.equals("params"))
			{
				String result = createParameter();
				out.println(result);
				log.warning(result + " triggered by " + LoginHelper.getLoggedInUser());
			}
			out.println("<b>setup done</b><br/>");
		}
		catch(DuplicateEntitiesException ex)
		{
			out.println("An error occured when creating objects: " + ex.getMessage());
		}
	}
		
	private static String createParameter() throws DuplicateEntitiesException
	{
		HashMap<String, String> params = new HashMap<String, String>();
		params.put(DTOConstants.MAX_PRINCIPAL_KEY, "750000");
		params.put(DTOConstants.MIN_PRINCIPAL_KEY, "100000");
		params.put(DTOConstants.MAX_TENOR_KEY, "6");
		params.put(DTOConstants.MIN_TENOR_KEY, "1");
		params.put(DTOConstants.INTEREST_KEY, "0.07");
		ApplicationParameters paramObj = LoanMktDAO.createApplicationParameters(EntityConstants.MARKETING_PARAM_ID, params);
		return "Created: " + paramObj.getKey();
	}
}
