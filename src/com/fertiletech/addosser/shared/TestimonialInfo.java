package com.fertiletech.addosser.shared;

public class TestimonialInfo {
	
	private String customerName;
	private String placeOfWork;
	private String ageRange;
	private String sex;
	private String testimonial;
	
	public TestimonialInfo(String customerName,String placeOfWork, String sex, String ageRange, String testimonial) {
		
		this.customerName = customerName;
		this.placeOfWork = placeOfWork;
		this.sex = sex;
		this.ageRange = ageRange;
		this.testimonial = testimonial;
	}
	
	
	//TODO
	// Hard coded information to be replaced later
	
	public static TestimonialInfo[] getTestimonials(){
		TestimonialInfo[] testInfo = new TestimonialInfo[3];
		testInfo[0] = new TestimonialInfo("Ebun", "Globacom", "Female", "20-30", "Amazing fast and efficient service, pleasant staff.");
		testInfo[1] = new TestimonialInfo("Lamidi", "Globacom", "Male", "31-40", "Helpful and also provides immediate answers [when you need money]");
		testInfo[2] = new TestimonialInfo("Tope", "Globacom", "Male", "31-40", "Quick and well guided service with less hassles [than I've experienced at other banks]");
		return testInfo;
	
	}

	public String getCustomerName() {
		return customerName;
	}

	public String getPlaceOfWork() {
		return placeOfWork;
	}

	public String getAgeRange() {
		return ageRange;
	}

	public String getSex() {
		return sex;
	}

	public String getTestimonial() {
		return testimonial;
	}
			
}
