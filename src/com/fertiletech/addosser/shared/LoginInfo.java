/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.shared;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Segun Razaq Sobulo
 */

public class LoginInfo implements Serializable {
    private boolean loggedIn = false;
	private String loginUrl;
    private String logoutUrl;
    private String name;
    private String loginID;
    private String message;
    private LoginRoles role;
    
    public LoginRoles getRole()
    {
    	return role;
    }
    
    public void setRole(LoginRoles role)
    {
    	this.role = role;
    }
    	
    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String displayName) {
        this.name = displayName;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String userKey) {
        this.loginID = userKey;
    }
}
