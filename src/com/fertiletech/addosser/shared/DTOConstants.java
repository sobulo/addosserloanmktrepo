/**
 * 
 */
package com.fertiletech.addosser.shared;

/**
 * @author Segun Razaq Sobulo
 *
 */
public final class DTOConstants {
    //loan mkt param strings
    public final static String MAX_PRINCIPAL_KEY = "XP";
    public final static String MIN_PRINCIPAL_KEY = "NP";
    public final static String MAX_TENOR_KEY = "XT";
    public final static String MIN_TENOR_KEY = "NT";
    public final static String INTEREST_KEY = "I";
}
