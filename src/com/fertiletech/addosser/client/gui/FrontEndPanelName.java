package com.fertiletech.addosser.client.gui;

import java.util.HashMap;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.client.LoanMktServiceAsync;
import com.fertiletech.addosser.client.admin.WebAppHelper;
import com.fertiletech.addosser.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class FrontEndPanelName extends Composite implements ClickHandler  {

	private static FrontEndPanelNameUiBinder uiBinder = GWT
			.create(FrontEndPanelNameUiBinder.class);

	interface FrontEndPanelNameUiBinder extends
			UiBinder<Widget, FrontEndPanelName> {
	}

	@UiField
	Button button;
	
	@UiField
	Label name;
	
	@UiField
	Label salary;
	
	@UiField
	Label job;
	
	@UiField
	Label loan;
	
	@UiField
	Label email;
	
	@UiField
	Label number;
	
	@UiField
	TextBox nameField;
	
	@UiField
	TextBox salaryField;
	
	@UiField
	TextBox jobField;
	
	@UiField
	TextBox loanField;
	
	@UiField
	TextBox emailField;
	
	@UiField
	TextBox numberField;
	
	@UiField
	HTMLPanel mainPanel;
	
	Integer minPrincipal;
	Integer maxPrincipal;
	
	private final static LoanMktServiceAsync MKT_SERVICE = GWT.create(LoanMktService.class);
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<String> saveCallback = new AsyncCallback<String>() {

        @Override
        public void onSuccess(String result) {
        	mainPanel.clear();
        	mainPanel.add(new HTML(result));
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to save your request, refresh browser and try again. Error was: " + caught.getMessage());
        }
    };
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> paramCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	maxPrincipal = Integer.parseInt(result.get(DTOConstants.MAX_PRINCIPAL_KEY));
        	minPrincipal = Integer.parseInt(result.get(DTOConstants.MIN_PRINCIPAL_KEY));
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve current param values. Try refreshing your browser. " +
        			"Contact infotech@addosser.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        	button.setEnabled(false);
        }
    };   
    
	
	public FrontEndPanelName() {
		initWidget(uiBinder.createAndBindUi(this));
		button.addClickHandler(this);
		MKT_SERVICE.getParameters(paramCallback);

		nameField.setName(name.getText());
		salaryField.setName(name.getText());
		jobField.setName(job.getText());
		loanField.setName(loan.getText());
		emailField.setName(email.getText());
		numberField.setName(number.getText());		
	}
	
	public FrontEndPanelName(double principal)
	{
		this();
		loanField.setText(String.valueOf((int) principal));
	}
	
	//check to ensure user data is ok
	private boolean validateInformation()
	{
		String display = "";
		String temp = validateNotEmpty(nameField);
		if(temp != null)
			display += "<li>" + temp + "</li>";
		temp = validateNotEmpty(jobField);
		if(temp != null)
			display += "<li>" + temp + "</li>";
		temp = validateNotEmpty(emailField);
		if(temp != null)
			display += "<li>" + temp + "</li>";
		temp = validateNotEmpty(numberField);
		if(temp != null)
			display += "<li>" + temp + "</li>";
		
		temp = validateNotEmpty(salaryField);
		if(temp == null)
			temp = validateNum(salaryField);
		if(temp != null)
			display += "<li>" + temp + "</li>";
		
		temp = validateNotEmpty(loanField);
		if(temp == null)
			temp = validateNum(loanField);
		if(temp != null)
			display += "<li>" + temp + "</li>";
		
		if(maxPrincipal == null || minPrincipal == null)
			display += "<li>Initialization not yet complete. Wait a few seconds then try again</li>";
		else
		{
		
			Double principal = Double.parseDouble(loanField.getText());
			if( principal < minPrincipal)
				display += "Requested loan amount, " + loanField.getText() + " can not be less than " + minPrincipal;
			if( principal > maxPrincipal )
				display += "Requested loan amount, " + loanField.getText() + " can not exceed " + maxPrincipal;
		}
		
		if(display.length() != 0)
		{
			PanelUtilities.errorBox.show("<ul>" + display + "</ul>");
			return false;
		}
		
		return true;
	}

	@Override
	public void onClick(ClickEvent event) {
		if(validateInformation())
		{
			MKT_SERVICE.createSaleLead(numberField.getText(), jobField.getText(), nameField.getText(), 
					Double.parseDouble(salaryField.getText()), emailField.getText(), 
					Double.parseDouble(loanField.getText()), saveCallback);
		}
	}
	
	private String validateNotEmpty(TextBox field)
	{
		String val = field.getText();
		if(val == null || (val = val.trim()).length() == 0 )
			return field.getName() + " must have a value";
		return null;
	}
	
	private String validateNum(TextBox field)
	{		
		try
		{
			Double.parseDouble(field.getText());
		}
		catch(NumberFormatException ex)
		{
			return field.getName() + " is not a number";
		}
		return null;
	}	
}
