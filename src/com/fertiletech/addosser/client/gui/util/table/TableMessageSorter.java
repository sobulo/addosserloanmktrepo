package com.fertiletech.addosser.client.gui.util.table;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Custom sorter for {@link Message} objects
 */
public class TableMessageSorter {

    /**
     * Sort the incoming map via the comparator.
     * @param map the map to sort
     * @param comparator the comparator to use
     * @return a sorted map.
     */
    public Map<Integer, TableMessage> sort(Map<Integer, TableMessage> map, Comparator<TableMessage> comparator) {
        final List<TableMessage> list = new LinkedList<TableMessage>(map.values());
        Collections.sort(list, comparator);
        int size = list.size();
        Map<Integer, TableMessage> result = new LinkedHashMap<Integer, TableMessage>(size);
        for(int i = 0; i < size; i++) {
            result.put(i, list.get(i));
        }
        return result;
    }

    public void sort(ArrayList<TableMessage> messageList, Comparator<TableMessage> comparator) {
        Collections.sort(messageList, comparator);
    }

    /**
     * {@link Comparator} for sorting by {@link Message} ID
     */
    public final static class NumberComparator implements Comparator<TableMessage> {

        private boolean ascending;
        private final int fieldIndex;

        public NumberComparator(boolean ascending, int fieldIndex) {
            this.ascending = ascending;
            this.fieldIndex = fieldIndex;
        }

        public int compare(TableMessage m1, TableMessage m2) {
            final Double val1 = m1.getNumber(fieldIndex);
            final Double val2 = m2.getNumber(fieldIndex);

            if(val1 == val2)
                return 0;
            if (ascending) 
            {
                if(val1 == null)
                    return -1;
                if(val2 == null)
                    return 1;
                return val1.compareTo(val2);
            }
            else
            {
                if(val1 == null)
                    return 1;
                if(val2 == null)
                    return -1;
                return val2.compareTo(val1);
            }
        }

        public void setAscending(boolean ascending)
        {
            this.ascending = ascending;
        }

        public int getFieldIndex()
        {
            return fieldIndex;
        }
    }

    /**
     * {@link Comparator} for sorting by {@link Message} TEXT
     */
    public final static class TextComparator implements Comparator<TableMessage> {

        private boolean ascending;
        private final int fieldIndex;

        public TextComparator(boolean ascending, int fieldIndex) {
            this.ascending = ascending;
            this.fieldIndex = fieldIndex;
        }

        public int compare(TableMessage m1, TableMessage m2) {
            final String val1 = m1.getText(fieldIndex);
            final String val2 = m2.getText(fieldIndex);
            if(val1 == val2)
                return 0;
            if (ascending)
            {
                if(val1 == null)
                    return -1;
                if(val2 == null)
                    return 1;
                return val1.compareTo(val2);
            }
            else
            {
                if(val1 == null)
                    return 1;
                if(val2 == null)
                    return -1;
                return val2.compareTo(val1);
            }
        }

        public void setAscending(boolean ascending)
        {
            this.ascending = ascending;
        }

        public int getFieldIndex()
        {
            return fieldIndex;
        }
    }

    /**
     * {@link Comparator} for sorting by {@link Message} DATE
     */
    public final static class DateComparator implements Comparator<TableMessage> {

        private boolean ascending;
        private final int fieldIndex;

        public DateComparator(boolean ascending, int fieldIndex) {
            this.ascending = ascending;
            this.fieldIndex = fieldIndex;
        }

        public int compare(TableMessage m1, TableMessage m2) {
            final Date val1 = m1.getDate(fieldIndex);
            final Date val2 = m2.getDate(fieldIndex);
            if(val1 == val2)
                return 0;
            if (ascending)
            {
                if(val1 == null)
                    return -1;
                if(val2 == null)
                    return 1;
                return val1.compareTo(val2);
            }
            else
            {
                if(val1 == null)
                    return 1;
                if(val2 == null)
                    return -1;
                return val2.compareTo(val1);
            }
        }

        public void setAscending(boolean ascending)
        {
            this.ascending = ascending;
        }

        public int getFieldIndex()
        {
            return fieldIndex;
        }
    }
}
