/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.addosser.client.gui.util.table;

import java.util.Date;

/**
 *
 * @author Segun Razaq Sobulo
 */
public class TableMessageHeader extends TableMessage{
    public enum TableMessageContent{TEXT, NUMBER, DATE};
    private TableMessageContent[] typeOrdering;
    private String[] groupNames;
    private boolean[] isEditable;
    private int typeCursor;
    private String caption;

    public TableMessageHeader()
    {
        super();
    }

    public TableMessageContent getHeaderType(int index)
    {
        return typeOrdering[index];
    }

    public int getNumberOfHeaders()
    {
        return typeOrdering.length;
    }
    
    public TableMessageHeader(int numOfHeaders)
    {
        super(numOfHeaders, 0, 0);
        typeOrdering = new TableMessageContent[numOfHeaders];
        isEditable = new boolean[numOfHeaders];
        groupNames = new String[numOfHeaders];
        typeCursor = 0;
    }

    
    public String getGroupName(int index) {
		return groupNames[index];
	}

	public void setGroupName(int index, String groupName) {
		groupNames[index] = groupName;
	}

	public void setText(int headerPosition, String val, TableMessageContent 
            headerContentType)
    {
        super.setText(headerPosition, val);
        typeOrdering[typeCursor] = headerContentType;
        typeCursor++;
    }
    
    public void setEditable(int headerPosition, boolean isEditable)
    {
    	TableMessageContent headerContentType = getHeaderType(headerPosition);
    	if(headerContentType == TableMessageContent.NUMBER)
    	{
    		this.isEditable[headerPosition] = isEditable;
    	}
    	else
    		throw new UnsupportedOperationException("Only number editing supported");
    }
    
    public boolean isEditable(int headerPosition)
    {
    	TableMessageContent headerContentType = getHeaderType(headerPosition);
    	if(headerContentType == TableMessageContent.NUMBER)
    	{
    		return this.isEditable[headerPosition];
    	}
    	else
    		throw new UnsupportedOperationException("Only number editing supported");    	
    }

    @Override
    public void setNumber(int index, Number val)
    {
        throw new UnsupportedOperationException("only texts supported");
    }

    @Override
    public void setDate(int index, Date val)
    {
        throw new UnsupportedOperationException("only texts supported");
    }

    @Override
    public void setText(int index, String val)
    {
        throw new UnsupportedOperationException("use setText(index, val, " +
                "TableMessageContent)");
    }

    public String getCaption()
    {
        return caption;
    }

    public void setCaption(String caption)
    {
        this.caption = caption;
    }
}
