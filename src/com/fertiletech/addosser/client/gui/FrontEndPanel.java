package com.fertiletech.addosser.client.gui;

import java.util.HashMap;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.client.LoanMktServiceAsync;
import com.fertiletech.addosser.client.admin.WebAppHelper;
import com.fertiletech.addosser.shared.DTOConstants;
import com.fertiletech.addosser.shared.TestimonialInfo;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class FrontEndPanel extends Composite implements ClickHandler, ValueChangeHandler<String>, KeyPressHandler {

	
	@UiField
	TextBox principalTextBox;
	
	@UiField
	Label monthlyPaymentDisplay;
			
	//@UiField
	//Label interestLabel;
	
	@UiField
	TextBox noOfPaymentsTextBox;
	
	@UiField
	Label principal;
	
	@UiField
	Label monthlyPayment;
	
	//@UiField
	//Label interest;
	
	@UiField
	Label noOfPayments;
	
	@UiField
	Button principalAdd;
	
	@UiField
	Button principalSubtract;
	
	@UiField
	Label error;
	
	@UiField
	FlexTable table;
	
	@UiField
	Button showTable;
	
	@UiField
	SimplePanel applySlot;
	
	@UiField
	Image imageAd;
	
	@UiField
	HTMLPanel rightImageAd;
	
	@UiField
	HTML testimonialScroll;
	
	@UiField
	Button principalSubtractMuch;
	
	@UiField
	Button principalAddPlenty;
	
	@UiField
	Button rightImageButton;
	
	FrontEndPanelName customerDetails;
	TestimonialInfo customerTestimonial;
	
	final static String APP_NOW_DISPLAY = "Pre-Apply Now";
	final static String APP_NOW_HIDE = "Hide Application";
	final static String PRODUCTS_PAGE = "http://www.addosser.com/products-services";
	
	static Integer maxLoanAmount;
	static Integer minLoanAmount;
	 
	static double INCREMENT_DECREMENT_VARIABLE = 10000;
	static double INCREMENT_DECREMENT_BY_MUCH = 75000;	 
	 
	static Integer maxTenor;
	static Integer minTenor;
	 
	final static int NEAREST_INT_TO_ROUND_TO = 1000;
	static Float interest;
	
	private final static LoanMktServiceAsync MKT_SERVICE = GWT.create(LoanMktService.class);
	 
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> initCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	maxLoanAmount = Integer.parseInt(result.get(DTOConstants.MAX_PRINCIPAL_KEY));
        	minLoanAmount = Integer.parseInt(result.get(DTOConstants.MIN_PRINCIPAL_KEY));
        	maxTenor = Integer.parseInt(result.get(DTOConstants.MAX_TENOR_KEY));
        	minTenor = Integer.parseInt(result.get(DTOConstants.MIN_TENOR_KEY));
        	interest = Float.parseFloat(result.get(DTOConstants.INTEREST_KEY));
        	enableEvents();
        	enableFields(true);
        	testimonialsInformation();
        	
        	//setup initial values
        	setPrincipalValue(minLoanAmount);
        	noOfPaymentsTextBox.setText(maxTenor.toString());
        	loanCalculation();
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve current param values. Try refreshing your browser. " +
        			"Contact infotech@addosser.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");

        }
    };   
	 
	private static FrontEndPanelUiBinder uiBinder = GWT
			.create(FrontEndPanelUiBinder.class);

	interface FrontEndPanelUiBinder extends UiBinder<Widget, FrontEndPanel> {
	}

	public FrontEndPanel() {
			initWidget(uiBinder.createAndBindUi(this));
			MKT_SERVICE.getParameters(initCallback);
			table.setVisible(false);
			testimonialScroll.setHTML("<h2>Loading parameters ... please wait</h2>");
	}
	
	public void enableEvents()
	{
		principalTextBox.addValueChangeHandler(this);
		noOfPaymentsTextBox.addValueChangeHandler(this);
		principalAdd.addClickHandler(this);
		principalSubtract.addClickHandler(this);		
		principalAddPlenty.addClickHandler(this);
		principalSubtractMuch.addClickHandler(this);
		imageAd.addClickHandler(this);
		rightImageButton.addClickHandler(this);		
		showTable.addClickHandler(this);
	}
	
	private void enableFields(boolean enable)
	{
    	principalAdd.setEnabled(enable);
    	principalAddPlenty.setEnabled(enable);
    	principalSubtract.setEnabled(enable);
    	principalSubtractMuch.setEnabled(enable);
    	principalTextBox.setEnabled(enable);
    	noOfPaymentsTextBox.setEnabled(enable);
    	showTable.setEnabled(enable);		
	}

	private void testimonialsInformation() {
		
		TestimonialInfo[] testimonialArray = TestimonialInfo.getTestimonials();
		String marqueeDisplay = "<table border='0'>";
		for(int i = 0; i < testimonialArray.length; i++)
		{	
			marqueeDisplay += "<tr><td align='left' style='font-size:18px'>" + testimonialArray[i].getTestimonial() + "</td></tr><tr><td align='right'>*Customer Name: "+ 
									  testimonialArray[i].getCustomerName() + ", Sex: " + testimonialArray[i].getSex() + ", Employed at: " + testimonialArray[i].getPlaceOfWork();
		}
		marqueeDisplay += "</td></tr></table>";
		testimonialScroll.setHTML(marqueeDisplay);

	}
	private void loanCalculation() {
	
		try 
		{
			double monthly_payment;
			error.setText("");
			
			double principal = getPrincipalValue();
			
						
			//String getInterest = interestLabel.getText();
			//double interest = Double.parseDouble(getInterest);
			
			String getPeriod = noOfPaymentsTextBox.getText();
			double period = Double.parseDouble(getPeriod);
			
			
			if ((principal >= minLoanAmount) && (principal <= maxLoanAmount)) {
				
				if ((period >= minTenor) && (period <= maxTenor)) {
					
					monthly_payment = principal * (interest / (1 - (Math.pow(1 + interest, -period))));
					
					//monthly_payment = roundNumberUp(monthly_payment);
					NumberFormat fmt = NumberFormat.getFormat("#,##0");
					String stringTotal = fmt.format(monthly_payment);
					
					monthlyPaymentDisplay.setText(stringTotal);
					
					double current_principal = principal;
					double principal_paid, interest_paid, beginning_balance;
					final double THRESH_HOLD = 0.01;
					
					table.removeAllRows();
					table.setCellPadding(6);
					table.getRowFormatter().addStyleName(0, "tableListHeader");
					table.addStyleName("tableList");
					table.getCellFormatter().addStyleName(0, 0, "tableListNumericColumn");
					table.getCellFormatter().addStyleName(0, 1, "tableListNumericColumn");
					table.getCellFormatter().addStyleName(0, 2, "tableListNumericColumn");
					table.getCellFormatter().addStyleName(0, 3, "tableListNumericColumn");
					
					table.setText(0, 0, "Beginning Balance");
					table.setText(0, 1, "Interest Paid");
					table.setText(0, 2, "Principal Paid");
					table.setText(0, 3, "Ending Balance");
					
					while (current_principal > THRESH_HOLD) {
						beginning_balance = current_principal;
						interest_paid = interest * current_principal;
						principal_paid = monthly_payment - interest_paid;
						current_principal = beginning_balance - principal_paid;
						
						String beginningBalance = fmt.format(beginning_balance);
						String interestPaid = fmt.format(interest_paid);
						String principalPaid = fmt.format(principal_paid);
						String currentPrincipal = fmt.format(current_principal);
						
						int row = table.getRowCount();
						
						table.getCellFormatter().addStyleName(row, 0, "tableIndividualColumn");
						table.getCellFormatter().addStyleName(row, 1, "tableIndividualColumn");
						table.getCellFormatter().addStyleName(row, 2, "tableIndividualColumn");
						table.getCellFormatter().addStyleName(row, 3, "tableIndividualColumn");
						
						table.setText(row, 0, beginningBalance);
						table.setText(row, 1, interestPaid);
						table.setText(row, 2, principalPaid);
						table.setText(row, 3, currentPrincipal);
					}
					
				} else {
					error.setText("NOTE: Please Enter Valid Numbers for the Principal Field and Number of Payments Field\n" +
							"Allowed Numbers are ₦100000 - ₦750000 for principal Field and 1 - 6 months of Number of Payments ");
				}
			} else {
				error.setText("NOTE: Please Enter Valid Numbers for the Principal Field and Number of Payments Field\n" +
						"Allowed Numbers are ₦100000 - ₦750000 for principal Field and 1 - 6 months of Number of Payments ");
			}
							
		}
		catch (Exception exception)
		{
			String errorStatus = "NOTE: Principal/Number of Payments fields can't be left empty and must be a figure";
			error.setText(errorStatus);
		}
		
		
	}
	
	private void setPrincipalValue(double amount)
	{
		//TODO replace other places where prinicpal is being set with this
		NumberFormat fmt = NumberFormat.getFormat("#,##0");
		String newPrincipal = fmt.format(amount);
		principalTextBox.setValue(String.valueOf(newPrincipal), true);		
	}
	
	private double getPrincipalValue()
	{
		//TODO have other places where principal retrieved call this function. Also do similar for other fields where same calc repeated often
		String getPrincipal =  principalTextBox.getText();
		double principal = NumberFormat.getFormat("#,##0.00").parse(getPrincipal);
		return principal;
	}
	
	public void principalSteppedChange(double stepAmount, boolean isAdd) 
	{
	
		double principal = getPrincipalValue();
		if(isAdd)
		{
			principal += stepAmount;
			if ( principal > maxLoanAmount ) {
				error.setText("NOTE: Principal can't exceed ₦" + maxLoanAmount );
				principal = maxLoanAmount;
			}
		}
		else
		{
			principal -= stepAmount;
			if ( principal < minLoanAmount ) {
				error.setText("NOTE: Principal can't be lesser than ₦" + minLoanAmount );
				principal = minLoanAmount;
			}			
		}
		principal = (double) roundNumberUp(principal, NEAREST_INT_TO_ROUND_TO);
		setPrincipalValue(principal);
	}
	
	public int roundNumberUp(double numberToRoundUp, int nearestInt) {
		numberToRoundUp = numberToRoundUp / nearestInt;
		numberToRoundUp = Math.round(numberToRoundUp);
		numberToRoundUp = numberToRoundUp * nearestInt;
		return ((int) numberToRoundUp);
	}
	
	@Override
	public void onClick(ClickEvent event) {
		
		error.setText("");
		
		if (event.getSource() == principalAdd )
			principalSteppedChange(INCREMENT_DECREMENT_VARIABLE, true);
		else if (event.getSource() == principalSubtract)
			principalSteppedChange(INCREMENT_DECREMENT_VARIABLE, false);
		else if (event.getSource() == principalAddPlenty) 
			principalSteppedChange(INCREMENT_DECREMENT_BY_MUCH, true);
		else if (event.getSource() == principalSubtractMuch) 
			principalSteppedChange(INCREMENT_DECREMENT_BY_MUCH, false);
		else if (event.getSource() == showTable) {
			
			if (!table.isVisible()) {
				table.setVisible(true);
				imageAd.setVisible(false);
				showTable.setText("Hide Schedule");
			}
			else if (table.isVisible()) {
				table.setVisible(false);
				imageAd.setVisible(true);
				showTable.setText("Show Schedule");
			}
	
		 }
		else if (event.getSource() == imageAd)
		{
			Window.open(PRODUCTS_PAGE, "_blank", null);
		}
		else if (event.getSource() == rightImageButton) {
			 	
				rightImageAd.setVisible(false);
				if(customerDetails == null)
					customerDetails = new FrontEndPanelName(getPrincipalValue());
				applySlot.add(customerDetails);
		}
		
	} 
	

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		loanCalculation();
	}

	@Override
	public void onKeyPress(KeyPressEvent event) {
		if (event.getCharCode() == KeyCodes.KEY_ENTER) {
				loanCalculation();
			
		}
		
	}
}
