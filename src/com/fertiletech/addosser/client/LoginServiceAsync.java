package com.fertiletech.addosser.client;

import com.fertiletech.addosser.shared.LoginInfo;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface LoginServiceAsync {
	void login(String requestUrl, AsyncCallback<LoginInfo> callback);
}
