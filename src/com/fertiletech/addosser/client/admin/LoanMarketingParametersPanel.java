/**
 * 
 */
package com.fertiletech.addosser.client.admin;

import java.util.ArrayList;
import java.util.HashMap;

import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.gui.PanelUtilities;
import com.fertiletech.addosser.shared.DTOConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class LoanMarketingParametersPanel extends Composite {

	@UiField
	Label minPrincipalLabel;
	
	@UiField
	Label maxPrincipalLabel;
	
	@UiField
	Label minTenorLabel;
	
	@UiField
	Label maxTenorLabel;

	@UiField
	Label interestLabel;	
	
	@UiField
	TextBox minPrincipal;
	
	@UiField
	TextBox maxPrincipal;
	
	@UiField
	TextBox minTenor;
	
	@UiField
	TextBox maxTenor;
	
	@UiField
	TextBox interestRate;
	
	@UiField
	Button save;
	
	private final static float maxInterest = 1;
	private final static float minInterest = 0;
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<Void> saveCallback = new AsyncCallback<Void>() {

        @Override
        public void onSuccess(Void result) {
        	//TODO add url to addosser.com in show message
        	PanelUtilities.infoBox.show("Values saved successfully. Web app on addosser.com" +
        			" will now reflect your changes");
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to save values. Error was: " + caught.getMessage());
        }
    };
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<HashMap<String, String>> paramCallback = new AsyncCallback<HashMap<String, String>>() {

        @Override
        public void onSuccess(HashMap<String, String> result) {
        	maxPrincipal.setText(result.get(DTOConstants.MAX_PRINCIPAL_KEY));
        	minPrincipal.setText(result.get(DTOConstants.MIN_PRINCIPAL_KEY));
        	maxTenor.setText(result.get(DTOConstants.MAX_TENOR_KEY));
        	minTenor.setText(result.get(DTOConstants.MIN_TENOR_KEY));
        	interestRate.setText(result.get(DTOConstants.INTEREST_KEY));
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve current param values. Try refreshing your browser. " +
        			"Contact info@fertiletech.com if problems persist. <p> Error msg: <b>"+caught.getMessage() + "</b></p>");
        	save.setEnabled(false);
        }
    };   
	
	private static LoanMarketingParametersPanelUiBinder uiBinder = GWT
			.create(LoanMarketingParametersPanelUiBinder.class);

	interface LoanMarketingParametersPanelUiBinder extends
			UiBinder<Widget, LoanMarketingParametersPanel> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public LoanMarketingParametersPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		initializeValues();
		
		/**
		 * Normally name fields are used to identify fields when submitted to a server side via
		 * a cgi form action. This is not needed for gwt rpc calls however we name the fields here
		 * to identify them for generic error handling on the textboxes (see validate fn) 
		 */
		minPrincipal.setName(maxPrincipalLabel.getText());
		maxPrincipal.setName(minPrincipalLabel.getText());
		minTenor.setName(minTenorLabel.getText());
		maxTenor.setName(maxTenorLabel.getText());
		interestRate.setName(interestLabel.getText());
		
		save.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(validate())
				{
					HashMap<String, String> params = new HashMap<String, String>();
					params.put(DTOConstants.MAX_PRINCIPAL_KEY, maxPrincipal.getText());
					params.put(DTOConstants.MIN_PRINCIPAL_KEY, minPrincipal.getText());
					params.put(DTOConstants.MAX_TENOR_KEY, maxTenor.getText());
					params.put(DTOConstants.MIN_TENOR_KEY, minTenor.getText());
					params.put(DTOConstants.INTEREST_KEY, interestRate.getText());
					WebAppHelper.LOAN_MKT_SERVICE.updateParameters(params, saveCallback);
				}
			}
		});
	}
	
	public void initializeValues()
	{
		WebAppHelper.LOAN_MKT_SERVICE.getParameters(paramCallback);
	}
	
	/**
	 * @return true if no errors otherwise returns false
	 */
	private boolean validate()
	{
		ArrayList<String> errors = new ArrayList<String>();
		
		String temp = validateInterest();
		if(temp != null) errors.add(temp);
		
		temp = validateNum(minPrincipal);
		if(temp != null) errors.add(temp);
		
		temp = validateNum(maxPrincipal);
		if(temp != null) errors.add(temp);
		
		temp = validateNum(minTenor);
		if(temp != null) errors.add(temp);
		
		temp = validateNum(maxTenor);
		if(temp != null) errors.add(temp);		
		
		if(errors.size() != 0)
		{
			String htmlMsg = "";
			for(String err : errors)
				htmlMsg += err + "<br/>";
			PanelUtilities.errorBox.show(htmlMsg);
			return false;
		}
		else
			return true;
	}
	
	/**
	 * @return null if valid number otherwise returns an error message
	 */
	private String validateNum(TextBox field)
	{
		String numStr = field.getText();
		if(numStr == null || (numStr = numStr.trim()).length() == 0 )
			return field.getName() + " must have a value";
		
		try
		{
			Integer.parseInt(numStr);
		}
		catch(NumberFormatException ex)
		{
			return field.getName() + " is not a number. Only numbers (no decimals/alphabets etc allowed for this field)";
		}
		return null;
	}
	
	private String validateInterest()
	{
		Float interest = null;
		String errors = null;
		try
		{
			interest = Float.parseFloat(interestRate.getText());
		}
		catch(NumberFormatException ex)
		{
			errors = interestRate.getName() + " should be a decimal number";
		}
		
		if(errors == null)
		{
			if(interest <= minInterest || interest > maxInterest)
				errors = interestRate.getName() + " must be between " + minInterest + " and " + maxInterest;
		}
		return errors;
	}

}
