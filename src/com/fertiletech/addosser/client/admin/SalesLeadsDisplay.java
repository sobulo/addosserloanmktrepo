/**
 * 
 */
package com.fertiletech.addosser.client.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fertiletech.addosser.client.gui.PanelUtilities;
import com.fertiletech.addosser.client.gui.util.table.PsTable;
import com.fertiletech.addosser.client.gui.util.table.TableMessage;
import com.fertiletech.addosser.client.gui.util.table.TableMessageHeader;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.google.gwt.user.datepicker.client.DateBox;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class SalesLeadsDisplay extends Composite {
	
	@UiField
	DateBox startDate;

	@UiField
	DateBox endDate;
	
	@UiField
	Button search;
	
	@UiField
	SimplePanel tableContent;
	
	private PsTable table;
	private final static HTML WAIT_DISPLAY = new HTML("Fetching data. Please wait ...");
	
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<List<TableMessage>> tableCallback = new AsyncCallback<List<TableMessage>>() {

        @Override
        public void onSuccess(List<TableMessage> result) {
        	GWT.log("Got back" + result.size());
        	TableMessageHeader header = (TableMessageHeader) result.remove(0);
        	if(table == null)
        		table = new PsTable(header);
        	table.showMessages((ArrayList<TableMessage>) result, "Please follow up on leads below");
        	tableContent.clear();
        	tableContent.add(table);
        }

        @Override
        public void onFailure(Throwable caught) {
        	PanelUtilities.errorBox.show("Unable to retrieve list of loan requests. Error was: " + caught.getMessage());
        }
    };   	
	
	private static SalesLeadsDisplayUiBinder uiBinder = GWT
			.create(SalesLeadsDisplayUiBinder.class);

	interface SalesLeadsDisplayUiBinder extends
			UiBinder<Widget, SalesLeadsDisplay> {
	}

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	 *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SalesLeadsDisplay() 
	{
		initWidget(uiBinder.createAndBindUi(this));
		Date startVal = new Date();
		Date endVal = new Date();
		CalendarUtil.addDaysToDate(startVal, -1);
		startDate.setValue(startVal);
		endDate.setValue(endVal);
		search.addClickHandler(new ClickHandler() {		
			@Override
			public void onClick(ClickEvent event) {
				tableContent.clear();
				tableContent.add(WAIT_DISPLAY);
				WebAppHelper.LOAN_MKT_SERVICE.getSaleLeads(startDate.getValue(), endDate.getValue(), tableCallback);
			}
		});
	}
}
