/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.client.admin;


import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.LoanMktService;
import com.fertiletech.addosser.client.LoanMktServiceAsync;
import com.fertiletech.addosser.client.LoginService;
import com.fertiletech.addosser.client.LoginServiceAsync;
import com.fertiletech.addosser.client.gui.util.SimpleDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Segun Razaq Sobulo
 * 
 * IMPORTANT: only use package access or private access in this class. This is to prevent
 * unnecessary loading of certain gwt files in parts of the app that are public
 * 
 * TODO run gwt code splitting analytic tools to explicitly confirm which classes get loaded by public 
 * sections of the app
 */
public class WebAppHelper {

    private static int indexCounter = 0;
    private final static int WELCOME_IDX = indexCounter++;
    private final static int LOAN_PARAMS_IDX = indexCounter++;
    private final static int SALES_LEAD_IDX = indexCounter++;

    private static HyperlinkedPanel[] appPanels = new HyperlinkedPanel[indexCounter];
    
    final static LoginServiceAsync READ_SERVICE = GWT.create(LoginService.class);
    final static LoanMktServiceAsync LOAN_MKT_SERVICE = GWT.create(LoanMktService.class);

    static HyperlinkedPanel[] initializePanels() {
        GWT.log("loading static block");
        //welcome panel
        appPanels[WELCOME_IDX] = new HyperlinkedPanel() {

            private Hyperlink link;
            private WelcomePanel welcomePanel;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Welcome", "welcome");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (welcomePanel == null) {
                    welcomePanel = new WelcomePanel();
                }
                return welcomePanel;
            }
        };
        
        appPanels[LOAN_PARAMS_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget paramPanel = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Loan Params", "params");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (paramPanel == null) {
                    paramPanel = new LoanMarketingParametersPanel();
                }
                return paramPanel;
            }
        };           
        
        appPanels[SALES_LEAD_IDX] = new HyperlinkedPanel() 
        {
            private Hyperlink link;
            private Widget paramPanel = null;

            @Override
            public Hyperlink getLink() {
                if (link == null) {
                    link = new Hyperlink("Pre-Loan Requests", "sale-leads");
                }
                return link;
            }

            @Override
            public Widget getPanelWidget() {
                if (paramPanel == null) {
                    paramPanel = new SalesLeadsDisplay();
                }
                return paramPanel;
            }
        };                   
        return appPanels;
    }
    
    static Hyperlink[] getMenuItems() {
        GWT.log("calling gettree");
        
        Hyperlink[] result = new Hyperlink[appPanels.length];
        
        //welcome menu item
        for(int i = 0; i < appPanels.length; i++)
        	result[i] = appPanels[i].getLink();
        return result;
    }

    static int getWelcomeScreenIndex() {
        return WELCOME_IDX;
    }
    
	static StackLayoutPanel getStackLayout(Widget[]widgets, String[] headers)
	{
		StackLayoutPanel result = new StackLayoutPanel(Unit.PX);
		//either this or add to a simple panel with size set
		result.setHeight(GUIConstants.DEFAULT_STACK_HEIGHT);
		result.setWidth(GUIConstants.DEFAULT_STACK_WIDTH);
		
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), 
					headers[i], GUIConstants.DEFAULT_STACK_HEADER);
		return result;
	}
	
	static TabLayoutPanel getTabLayout(Widget[] widgets, String[]headers)
	{
		TabLayoutPanel result = new TabLayoutPanel(GUIConstants.DEFAULT_TAB_HEADER, Unit.PX);
		result.setHeight(GUIConstants.DEFAULT_TAB_HEIGHT);
		result.setWidth(GUIConstants.DEFAULT_TAB_WIDTH);		
		for(int i = 0; i < widgets.length; i++)
			result.add(new ScrollPanel(widgets[i]), headers[i]);
		return result;
	}	
}
