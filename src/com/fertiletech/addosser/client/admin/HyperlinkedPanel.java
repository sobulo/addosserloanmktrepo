/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.fertiletech.addosser.client.admin;

import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public interface HyperlinkedPanel {
    public Hyperlink getLink();
    public Widget getPanelWidget();
}
