package com.fertiletech.addosser.client.admin;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fertiletech.addosser.client.GUIConstants;
import com.fertiletech.addosser.client.LoginServiceAsync;
import com.fertiletech.addosser.shared.LoginInfo;
import com.fertiletech.addosser.shared.LoginRoles;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.LayoutPanel;
import com.google.gwt.user.client.ui.ResizeComposite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class WebAppPanel extends ResizeComposite implements ValueChangeHandler<String> 
{
    @UiField
    HorizontalPanel navcontent;
    @UiField
    LayoutPanel content;
    
    private final static String ADMIN_PAGE_URL_PREFIX = GWT.getHostPageBaseURL() + "loanmarketingadmin.html";
    private static WebAppPanelUiBinder uiBinder = GWT.create(WebAppPanelUiBinder.class);
    private final static String ADMIN_PAGE_URL;
    static
    {
    	if(!GWT.isProdMode()) //true if dev mode
    		ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX + GUIConstants.GWT_CODE_SERVER;
    	else
    		ADMIN_PAGE_URL = ADMIN_PAGE_URL_PREFIX;
    }
    


    //different views
    Widget currentContent; //keeps track of view currently displayed
    HyperlinkedPanel[] appPanels;
    WelcomePanel welcomePanel;
    
    String lastRequestedPage = "";

    interface WebAppPanelUiBinder extends UiBinder<Widget, WebAppPanel> {
    }
    
    
    // Create an asynchronous callback to handle the result.
    final AsyncCallback<LoginInfo> loginCallback = new AsyncCallback<LoginInfo>() {

        @Override
        public void onSuccess(LoginInfo result) {
        	//Show server message
        	String displayMessage = result.getMessage();
            welcomePanel.setDisplayHTML(displayMessage);
            
            if(result.getRole() != LoginRoles.ROLE_PUBLIC)
            {
	            String url = result.isLoggedIn() ? result.getLogoutUrl() : result.getLoginUrl();
	            setupMenu(result.isLoggedIn(), url);
            }
        }

        @Override
        public void onFailure(Throwable caught) {
            String displayMessage = "Unable to reach server." +
                    " Error was: [" + caught.getMessage() + "]<br/>";
            displayMessage += setupLink(ADMIN_PAGE_URL, "Click here to retry");
            welcomePanel.setDisplayHTML(displayMessage);
        }
    };

    final AsyncCallback<LoginInfo> ensureLoginCallback = new AsyncCallback<LoginInfo>() {

        @Override
        public void onSuccess(LoginInfo result) {
        	GWT.log("Ensure callback");
            if (!result.isLoggedIn()) {
                Window.alert("You've been signed out. For security/privacy reasons the system logs you out after " +
                        "an hour of inactivity. You'll need to login again");

                //since logged out on server, immediately clear client state
                //Window.Location.replace(ADMIN_PAGE_URL);
            }
            else if(result.getRole().equals(LoginRoles.ROLE_PUBLIC))
            	Window.alert("You do not have necessary privileges to view this page. Contact infotech@addosser.com" +
            			" or try logging out and then logging in with a different account");
            else
                loadNewPanel();
        }

        @Override
        public void onFailure(Throwable caught) {
            String displayMessage = "Unable to reach server." +
            " Error was: [" + caught.getMessage() + "]<br/>";
            displayMessage += setupLink(ADMIN_PAGE_URL, "Click here to retry");
            welcomePanel.setDisplayHTML(displayMessage);
        }
    };
    
    private String setupLink(String url, String display)
    {
    	return "<a href='" + url + "'>" + display + "</a>"; 
    }

    private void setupMenu(boolean loggedIn, String url) {
        GWT.log("adding menu");
        navcontent.add(new Label("MENU       "));
        
        if (loggedIn)
        {
        	//add panel links
            Hyperlink[] menuItems = WebAppHelper.getMenuItems();
            for(Hyperlink item : menuItems)
            	navcontent.add(item);
            
            //add link for logout
        	navcontent.add(new Anchor("Logout", url));
        	navcontent.setSpacing(GUIConstants.MENU_SPACING);
        }
        else 
        	navcontent.add(new Anchor("Login", url));
    }

    public WebAppPanel() {
        //GWT.log("initializing webapp panel");
        initWidget(uiBinder.createAndBindUi(this));
        
        //setup welcome screen
        int welcomeIndex = WebAppHelper.getWelcomeScreenIndex();

        appPanels = WebAppHelper.initializePanels();

        //GWT.log("initialized panels");
        currentContent = appPanels[welcomeIndex].getPanelWidget();
        
        //GWT.log("retrieved current content");
        welcomePanel = (WelcomePanel) currentContent;

        //GWT.log("referenced welcome panel");
        History.fireCurrentHistoryState();
        History.addValueChangeHandler(this);
        content.add(currentContent);
        WebAppHelper.READ_SERVICE.login(ADMIN_PAGE_URL, loginCallback);
    }

    @Override
    public void onValueChange(ValueChangeEvent<String> event) {
    	GWT.log("value change event received: " + event.getValue());
    	lastRequestedPage = event.getValue();
    	WebAppHelper.READ_SERVICE.login(ADMIN_PAGE_URL, ensureLoginCallback);
    	//check that user is still logged in. Is this overkill? 
    	//Should we just check once in the constructor vs everytime a new panel is loaded?
    }

    private void loadNewPanel()
    {
    	String newToken = lastRequestedPage;
        Widget newContent = null;
        
        //determine what widget user link points to
        for (HyperlinkedPanel cursorPanel : appPanels) {
            if (newToken.equals(cursorPanel.getLink().getTargetHistoryToken())) {
            		newContent = cursorPanel.getPanelWidget();
            }
        }

        //update current widget to widget specified by user above
        if (newContent != null && currentContent != newContent) {
            content.clear();
            content.add(newContent);
            currentContent = newContent;
        }
    }
}
