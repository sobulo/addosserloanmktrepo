/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fertiletech.addosser.client.admin;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author Administrator
 */
public class WelcomePanel extends Composite
{	
	@UiField
	HTML displayMessage;

    
    private static WelcomePanelUiBinder uiBinder = GWT.create(WelcomePanelUiBinder.class);
    
    interface WelcomePanelUiBinder extends UiBinder<Widget, WelcomePanel> {
    }
    
    
    public WelcomePanel() {
        initWidget(uiBinder.createAndBindUi(this));
    }
    
    public void setDisplayHTML(String htmlMessage)
    {
    	//TODO call setHTML with a Safehtml object instead of a string
    	displayMessage.setHTML(htmlMessage);
    }
}