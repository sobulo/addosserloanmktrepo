package com.fertiletech.addosser.client;

import com.fertiletech.addosser.client.admin.WebAppPanel;
import com.fertiletech.addosser.client.gui.FrontEndPanel;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.RunAsyncCallback;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class LoanMarketing implements EntryPoint {
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		final Panel rootPanel = RootPanel.get(GUIConstants.ROOT_ID_PUBLIC);
		if( rootPanel != null )
		{
			FrontEndPanel contentPanel = new FrontEndPanel();
			rootPanel.getElement().setInnerHTML("");//add(panel);
			rootPanel.add(contentPanel);
		}
		else
		{
			GWT.runAsync(new RunAsyncCallback() {
				
				@Override
				public void onSuccess() {
					// TODO Auto-generated method stub
					//clear the panel
					RootPanel tempPanel = RootPanel.get(GUIConstants.ROOT_ID_ADMIN);
					tempPanel.getElement().setInnerHTML("");
			
					//refetch the panel using the more advanced layout panel
					RootLayoutPanel rootLayoutPanel = RootLayoutPanel.get();
					WebAppPanel contentPanel = new WebAppPanel();
					rootLayoutPanel.add(contentPanel);
				}
				
				@Override
				public void onFailure(Throwable reason) {
					Window.alert("Code download failed, try refreshing the page." +
							" Please contact info@fertiletech.com if problem persists");
					
				}
			});
		}		
		
	}
	
	/* to call the function: clean(rootPanel)
	 * public static void clean(Element parent)
	{
	  if (parent == null)
	    throw new IllegalArgumentException();

	  Element firstChild;
	  while((firstChild = DOM.getFirstChild(parent)) != null)
	  {
	    DOM.removeChild(parent, firstChild);
	  }
	} */
}
