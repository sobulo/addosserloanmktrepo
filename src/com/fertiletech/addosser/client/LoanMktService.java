/**
 * 
 */
package com.fertiletech.addosser.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.client.gui.util.table.TableMessage;
import com.fertiletech.addosser.shared.exceptions.MissingEntitiesException;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * @author Segun Razaq Sobulo
 *
 */
@RemoteServiceRelativePath("admin/loanmkt")
public interface LoanMktService extends RemoteService {
	String createSaleLead(String phoneNumber, String companyName,
			String fullName, Double monthlySalary, String email, Double requestedAmount);
	void updateParameters(HashMap<String, String> parameters) throws MissingEntitiesException;
	HashMap<String, String> getParameters();
	List<TableMessage> getSaleLeads(Date startDate, Date endDate);
}
