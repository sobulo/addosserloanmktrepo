/**
 * 
 */
package com.fertiletech.addosser.client;

/**
 * @author Segun Razaq Sobulo
 *
 */
public class GUIConstants {
	public final static String STYLE_PS_TABLE_SPACER = "scrollTableSpacer"; 
	public final static String ROOT_ID_PUBLIC = "contentArea";
	public final static String ROOT_ID_ADMIN = "contentAreaAdmin";

    public final static String DEFAULT_STACK_WIDTH = "100%";
    public final static String DEFAULT_STACK_HEIGHT = "100%";  
    public final static Double DEFAULT_STACK_HEADER = 30.0;
    
    public final static String DEFAULT_TAB_WIDTH = "100%";
    public final static String DEFAULT_TAB_HEIGHT = "600px";  
    public final static Double DEFAULT_TAB_HEADER = 40.0;
    
    public final static int MENU_SPACING = 20;
    
    /**
     * urgh!!! different developers, different ports? equals commits containing code
     * server configuration. Hopefully everyone agrees to use same port configs
     */
    public final static String GWT_CODE_SERVER = "?gwt.codesvr=127.0.0.1:9997";
}
