/**
 * 
 */
package com.fertiletech.addosser.client;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.fertiletech.addosser.client.gui.util.table.TableMessage;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author Segun Razaq Sobulo
 *
 */
public interface LoanMktServiceAsync {

	/**
	 * 
	 * @see com.fertiletech.addosser.client.LoanMktService#createSaleLead(java.lang.String, java.lang.String, java.lang.String, java.lang.Double, java.lang.String, java.lang.Double)
	 */
	void createSaleLead(String phoneNumber, String companyName,
			String fullName, Double monthlySalary, String email,
			Double requestedAmount, AsyncCallback<String> callback);

	/**
	 * 
	 * @see com.fertiletech.addosser.client.LoanMktService#getParameters()
	 */
	void getParameters(AsyncCallback<HashMap<String, String>> callback);

	/**
	 * 
	 * @see com.fertiletech.addosser.client.LoanMktService#getSaleLeads(java.util.Date, java.util.Date)
	 */
	void getSaleLeads(Date startDate, Date endDate,
			AsyncCallback<List<TableMessage>> callback);

	/**
	 * 
	 * @see com.fertiletech.addosser.client.LoanMktService#updateParameters(java.util.HashMap)
	 */
	void updateParameters(HashMap<String, String> parameters,
			AsyncCallback<Void> callback);

}
